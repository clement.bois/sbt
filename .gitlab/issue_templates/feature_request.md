## Description

(Describe the feature clearly and concisely.)

## Implementation ideas

(If you have any implementation ideas, they can go here.)
(Any design change proposal could be also discussed on the _to be continuous_ Google Group: https://groups.google.com/g/tbc-dev.)


/label ~"kind/enhancement" ~"status/needs-investigation"
